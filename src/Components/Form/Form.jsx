import React from "react";
import { Typography } from "@mui/material";
import BaseContainer from "../BaseContainer";

export default function Forms() {
  return (
    <div>
      <BaseContainer>
        <Typography paragraph>Forms</Typography>
      </BaseContainer>
    </div>
  );
}
