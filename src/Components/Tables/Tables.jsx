import React from "react";
import { Typography } from "@mui/material";
import BaseContainer from "../BaseContainer";

export default function Tables() {
  return (
    <div>
      <BaseContainer>
        <Typography paragraph>Tables</Typography>
      </BaseContainer>
    </div>
  );
}
