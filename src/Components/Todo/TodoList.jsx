import React from "react";
import { Typography } from "@mui/material";
import BaseContainer from "../BaseContainer";

export default function TodoList() {
  return (
    <div>
      <BaseContainer>
        <Typography paragraph>TodoList</Typography>
      </BaseContainer>
    </div>
  );
}
